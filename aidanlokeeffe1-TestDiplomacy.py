# ---------------------------
# Aidan O'Keeffe, alo779
# Nan Shelden, nls2234
# Project 2: Diplomacy
# CS 330E: Elements of Software Engineering
# Created 7/6/2020
# Submitted 7/16/2020
# ---------------------------

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_solve, diplomacy_print
from Diplomacy import Army

# -----------
# Testdiplomacy
# -----------


class TestDiplomacy (TestCase):
    # ----
    # read : used to read each line of the file
    # ----

    def test_read_1(self):
        r = StringIO("A Madrid Hold\n")
        a = diplomacy_read(r)
        self.assertEqual(a, ["A Madrid Hold\n"])

    def test_read_2(self):
        r = StringIO("B Barcelona Move Madrid\n")
        a = diplomacy_read(r)
        self.assertEqual(a, ["B Barcelona Move Madrid\n"])

    def test_read_3(self):
        r = StringIO("C London Support B\n")
        a = diplomacy_read(r)
        self.assertEqual(a, ["C London Support B\n"])

    def test_read_4(self):
        r = StringIO("D NewYork Move Chicago\nC London Support B\n")
        a = diplomacy_read(r)
        self.assertEqual(a, ["D NewYork Move Chicago\n", "C London Support B\n"])

    # ----
    # eval  : will return results of armies in an ordered tuple
    # ----

    def test_eval_1(self):
        v = diplomacy_eval(["A Rome Move Madrid"])
        self.assertEqual(str(v), "[A Madrid]")

    def test_eval_2(self):
        v = diplomacy_eval(["A London Support B", "B Seattle Move Rio", "C Rio Move London"])
        self.assertEqual(str(v), "[A [dead], B Rio, C [dead]]")

    def test_eval_3(self):
        v = diplomacy_eval(["A Paris Move Berlin", "B Berlin Move Paris"])
        self.assertEqual(str(v), "[A Berlin, B Paris]")

    def test_eval_4(self):
        v = diplomacy_eval(["A Seattle Move Rio", "B London Support A", "C Rio Move London"])
        self.assertEqual(str(v), "[A Rio, B [dead], C [dead]]")


    # -----
    # print : prints the result of each army individually
    # -----

    def test_print_1(self):
        w = StringIO()
        result = [Army("A", "Austin")]
        diplomacy_print(w, result)
        self.assertEqual(w.getvalue(), "A Austin\n")

    def test_print_2(self):
        w = StringIO()
        result = [Army("A", "Austin"), Army("B", "Boston")]
        diplomacy_print(w, result)
        self.assertEqual(w.getvalue(), "A Austin\nB Boston\n")

    def test_print_3(self):
        w = StringIO()
        result = [Army("A", "Austin"), Army("B", "Boston"), Army("C", "Casten"), Army("D", "[dead]")]
        diplomacy_print(w, result)
        self.assertEqual(w.getvalue(), "A Austin\nB Boston\nC Casten\nD [dead]\n")

    def test_print_4(self):
        w = StringIO()
        result = [Army("as;dkjfha;skdfj", "I can print ANYTHING!!!!!")]
        diplomacy_print(w, result)
        self.assertEqual(w.getvalue(), "as;dkjfha;skdfj I can print ANYTHING!!!!!\n")

    # -----
    # solve : utilizes other functions to display total results
    # -----

    def test_solve_1(self):
        r = StringIO("A Madrid Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\n")

    def test_solve_2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC London\n")

    def test_solve_3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\n")

    def test_solve_4(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")

    def test_solve_5(self):
        r = StringIO("A Boston Support B\nB Austin Move Tenochtitlan\nC Tenochtitlan Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Boston\nB Tenochtitlan\nC [dead]\n")

    def test_solve_6(self):
        r = StringIO("A London Support B\nB Seattle Move Rio\n C Rio Move London\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Rio\nC [dead]\n")

    def test_solve_7(self):
        r = StringIO("A Paris Move Berlin\nB Berlin Move Paris\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Berlin\nB Paris\n")

    def test_solve_8(self):
        r = StringIO("A Sparta Support Athens\nB Athens Move Sparta")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\n")

    def test_solve_9(self):
        r = StringIO("A London Move Berlin\nB Washington Move Berlin\nC Moscow Move Berlin")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\n")

    def test_solve_10(self):
        r = StringIO("A Seattle Move Rio\nB London Support A\n C Rio Move London\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Rio\nB [dead]\nC [dead]\n")

    def test_solve_11(self):
        r = StringIO("A Rome Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\n")

    def test_solve_12(self):
        r = StringIO("A Tokyo Support B\nB Beijing Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Tokyo\nB Beijing\n")


# ----
# main
# ----

if __name__ == "__main__":
    main()
